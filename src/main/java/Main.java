import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.apache.http.client.utils.URIBuilder;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Optional;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String clubName = getClubNameFromUserInput();
        try {
            String pageUrl = getWikiUrlByClubName(clubName);
            System.out.println(pageUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getClubNameFromUserInput() {
        System.out.println("Input club name:");
        Scanner scan = new Scanner(System.in);

        return scan.next();
    }

    private static String getWikiUrlByClubName(String clubName) throws Exception {
        URL url = Optional.of(getUrl(clubName)).orElseThrow(Exception::new);
        String jsonString = getApiResponse(url);
        JSONArray jsonArray = JsonPath.parse(jsonString).read("$.query.search");
        String pageid = jsonArray.stream()
                .map(e -> (LinkedHashMap) e)
                .filter(e -> e.get("snippet").toString().toLowerCase().contains("football"))
                .map(e -> e.get("pageid").toString())
                .findFirst().orElse("0");

        return "https://en.wikipedia.org/?curid=" + pageid;
    }

    private static String getApiResponse(URL url) throws Exception {
        HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory();
        HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(url));

        return request.execute().parseAsString();
    }

    private static URL getUrl(String searchQuery) throws URISyntaxException, MalformedURLException {
        URIBuilder uriBuilder = new URIBuilder("https://en.wikipedia.org/w/api.php");
        uriBuilder.addParameter("action", "query");
        uriBuilder.addParameter("list", "search");
        uriBuilder.addParameter("format", "json");
        uriBuilder.addParameter("srsearch", searchQuery);
        uriBuilder.addParameter("srlimit", "10");

        return uriBuilder.build().toURL();
    }
}
